import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SidenavService {

  // tslint:disable-next-line:variable-name
  private readonly _sidenavOpened = new BehaviorSubject<boolean>(false);

  readonly sidenavOpened$ = this._sidenavOpened.asObservable();

  constructor() {
  }

  get sidenavOpened(): boolean {
    return this._sidenavOpened.getValue();
  }

  set sidenavOpened(value: boolean) {
    this._sidenavOpened.next(value);
  }
}
