import { Component, OnInit } from '@angular/core';
import { SidenavService } from '../../services/sidenav.service';
import { MenuContext } from '../../models/menu-context.model';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  readonly menuContext = MenuContext;

  constructor(public sidenavService: SidenavService) { }

  ngOnInit() {
  }

  onSidenavClose() {
    this.sidenavService.sidenavOpened = false;
  }

}
