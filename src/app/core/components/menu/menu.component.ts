import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MenuItem } from '../../models/menu-item.model';
import { MenuContext } from '../../models/menu-context.model';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SidenavService } from '../../services/sidenav.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  readonly menuItems: MenuItem[] = [
    {
      name: 'Strona główna',
      route: '',
    },
    {
      name: 'Wiadomości',
      route: '/messages'
    },
    {
      name: 'Departamenty',
      route: '/departments'
    },
    {
      name: 'Niezbędnik',
      route: '/toolbox'
    },
    {
      name: 'Ogłoszenia',
      route: '/announcements'
    },
    {
      name: 'Sekcje',
      route: '/sections'
    }
  ];

  @Input() context: MenuContext;

  private subs: Subscription[] = [];

  constructor(
    private router: Router,
    private sidenavService: SidenavService) {
  }

  ngOnInit() {
    this.subs.push(
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd)
        )
        .subscribe(event => {
          this.sidenavService.sidenavOpened = false;
        })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  get isToolbarContext(): boolean {
    return this.context === MenuContext.TOOLBAR;
  }

  get isMobileMenuContext(): boolean {
    return this.context === MenuContext.MOBILE_MENU;
  }

}
