import { Component } from '@angular/core';
import { MenuContext } from '../../models/menu-context.model';
import { SidenavService } from '../../services/sidenav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  readonly menuContext = MenuContext;

  constructor(private sidenavService: SidenavService) {
  }

  onMenuOpen() {
    this.sidenavService.sidenavOpened = true;
  }

}
