import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './core/views/not-found/not-found.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/toolbox',
    pathMatch: 'full'
  },
  {
    path: 'toolbox',
    loadChildren: () => import('./modules/toolbox/toolbox.module').then(m => m.ToolboxModule)
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
