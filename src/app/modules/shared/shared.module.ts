import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputDirective } from './directives/input.directive';
import { IconButtonDirective } from './directives/icon-button.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputErrorComponent } from './components/input-error/input-error.component';
import { ButtonDirective } from './directives/button.directive';

const directives = [
  InputDirective,
  IconButtonDirective,
];

const components = [
  InputErrorComponent
];


@NgModule({
  declarations: [
    ...directives,
    ...components,
    ButtonDirective,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    ...directives,
    ...components,
    ButtonDirective,
  ]
})
export class SharedModule { }
