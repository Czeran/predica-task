import { Directive, EventEmitter, HostBinding, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { InputColor } from '../models/input-color.enum';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appInput]'
})
export class InputDirective implements OnInit, OnDestroy {

  private clean = true;
  private subs: Subscription[] = [];

  @Output() hasError = new EventEmitter<boolean>();

  @Input() color: InputColor = InputColor.PRIMARY;

  @HostBinding('class.app-input') inputClass = true;

  @HostBinding('class.app-input--primary')
  get isPrimary(): boolean {
    return this.color === InputColor.PRIMARY;
  }

  @HostBinding('class.app-input--white')
  get isWhite(): boolean {
    return this.color === InputColor.WHITE;
  }

  @HostBinding('class.app-input--invalid')
  get isInvalid(): boolean {
    return this.control.invalid && !this.clean;
  }

  @HostListener('blur')
  onBlur() {
    if (!this.control.pristine) {
      this.clean = false;
      this.emitHasError();
    }
  }

  @HostListener('keyup.enter')
  onKeyUp() {
    this.clean = false;
  }

  constructor(
    private control: NgControl) {
  }

  ngOnInit() {
    this.subs.push(
      this.control.valueChanges
        .subscribe(() => this.emitHasError())
    );
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  private emitHasError() {
    this.hasError.emit(this.isInvalid);
  }
}
