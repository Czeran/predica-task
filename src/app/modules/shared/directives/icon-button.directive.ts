import { Directive, HostBinding, Input } from '@angular/core';
import { ButtonColor } from '../models/button-color.enum';

@Directive({
  selector: '[appIconButton]'
})
export class IconButtonDirective {

  @Input() color: ButtonColor = ButtonColor.PRIMARY;

  constructor() {
  }

  @HostBinding('class.app-icon-button') buttonClass = true;

  @HostBinding('class.app-icon-button--primary')
  get isPrimary(): boolean {
    return this.color === ButtonColor.PRIMARY;
  }

  @HostBinding('class.app-icon-button--white')
  get isWhite(): boolean {
    return this.color === ButtonColor.WHITE;
  }
}
