import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appButton]'
})
export class ButtonDirective {

  constructor() {
  }

  @HostBinding('class.app-button') appButtonClass = true;
}
