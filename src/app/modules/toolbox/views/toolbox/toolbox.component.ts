import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ToolboxService } from '../../services/toolbox.service';
import { ToolboxCategory } from '../../models/toolbox-category.model';
import { CategoryCardComponent } from '../../components/category-card/category-card.component';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.scss']
})
export class ToolboxComponent implements OnInit, OnDestroy {

  @ViewChildren(CategoryCardComponent) categoriesComponents: QueryList<CategoryCardComponent>;

  allCategories: ToolboxCategory[] = [];
  // tslint:disable-next-line:variable-name
  _filteredCategories: ToolboxCategory[] = [];
  categoryHeight: number;
  searchForm: FormGroup;
  invalidInputError = false;
  noResultError = false;
  searchString: string;
  categoriesFiltered = false;

  private subs: Subscription[] = [];

  constructor(
    private toolboxService: ToolboxService,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.createSearchForm()
      .then(() => {
        this.subs.push(this.searchForm.valueChanges.subscribe(() => {
          this.noResultError = false;
        }));
      });
    this.getCategories();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  search() {
    if (this.searchForm.invalid) {
      this.invalidInputError = true;
      return;
    }

    const {value: searchString} = this.searchForm.controls.search;

    if (!searchString) {
      this.clearSearchResults();
      return;
    }

    this.searchString = searchString;
    this.filterCategories();
  }

  searchInputHasError(hasError: boolean) {
    this.invalidInputError = hasError;
  }

  clearSearchResults() {
    this.searchString = null;
    this.searchForm.controls.search.setValue('');
    this.filteredCategories = this.allCategories;
    this.categoriesFiltered = false;
  }

  getErrorMessage() {
    if (this.invalidInputError) {
      return 'Wymane są minimum 3 znaki';
    } else if (this.noResultError) {
      return 'Dla podanej frazy nie znaleziono wyników';
    } else {
      return '';
    }
  }

  private createSearchForm(): Promise<void> {
    this.searchForm = this.fb.group({
      search: new FormControl('', [Validators.minLength(3)])
    });

    return Promise.resolve();
  }

  private getCategories() {
    this.toolboxService.getCategories$()
      .subscribe(categories => {
        this.allCategories = categories;
        this._filteredCategories = categories;
        this.setCategoriesHeight();
      });
  }

  private filterCategories() {
    const filteredCategoriesByCategoryName: ToolboxCategory[] = [];
    let restCategories: ToolboxCategory[] = [];
    const {search: searchControl} = this.searchForm.controls;

    this.allCategories.forEach(category => {
      if (category.name.toLowerCase().indexOf(this.searchString.toLowerCase()) > -1) {
        filteredCategoriesByCategoryName.push({...category});
      } else {
        restCategories.push({...category});
      }
    });

    restCategories = restCategories.map(category => {
      category.links = category.links.filter(link => link.displayName.toLowerCase().indexOf(this.searchString.toLowerCase()) > -1);
      return category;
    }).filter((category => category.links.length > 0));

    const allFilteredCategories = [...filteredCategoriesByCategoryName, ...restCategories];

    if (allFilteredCategories.length === 0) {
      this.noResultError = true;
      searchControl.setErrors({noResult: true});
      return;
    } else {
      this.noResultError = false;
      searchControl.setErrors(null);
      this.categoriesFiltered = true;
    }

    this.filteredCategories = allFilteredCategories;
  }

  private getHighestCategoryHeightValue(): number {
    let highestCategoryHeightValue = 0;
    this.categoriesComponents.forEach(category => {
      if (category.categoryHeight > highestCategoryHeightValue) {
        highestCategoryHeightValue = category.categoryHeight;
      }
    });

    return highestCategoryHeightValue;
  }

  private setCategoriesHeight() {
    setTimeout(() => {
      this.categoryHeight = this.getHighestCategoryHeightValue();
    });
  }

  get filteredCategories(): ToolboxCategory[] {
    return this._filteredCategories;
  }

  set filteredCategories(value: ToolboxCategory[]) {
    this._filteredCategories = value;
    this.categoryHeight = null;
    this.setCategoriesHeight();
  }
}
