export interface ToolboxCategoryLink {
  id: string;
  displayName: string;
  address: string;
}
