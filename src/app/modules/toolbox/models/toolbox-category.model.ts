import { ToolboxCategoryLink } from './toolbox-category-link.model';

export interface ToolboxCategory {
  id: string;
  name: string;
  icon: string;
  links: ToolboxCategoryLink[];
}
