import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlight'
})
export class HighlightPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const [searchString] = args;
    if (!searchString) {
      return value;
    }

    const searchStringLength = searchString.length;
    const occurrences = [];
    const searchStringCopy = searchString.slice().toLowerCase();
    const valueCopy = value.slice().toLowerCase();
    let startIndex = 0;
    let index;
    let indexShift = 0;

    // tslint:disable-next-line:no-conditional-assignment
    while ((index = valueCopy.indexOf(searchStringCopy, startIndex)) > -1) {
      occurrences.push(index);
      startIndex = index + searchStringLength;
    }

    occurrences.forEach(occurrence => {
      value = value.splice(occurrence + indexShift, 0, '<span class="highlight">');
      indexShift += 24;
      value = value.splice(occurrence + searchStringLength + indexShift, 0, '</span>');
      indexShift += 7;
    });

    return value;
  }

}
