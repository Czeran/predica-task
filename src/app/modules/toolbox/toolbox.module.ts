import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolboxRoutingModule } from './toolbox-routing.module';
import { ToolboxComponent } from './views/toolbox/toolbox.component';
import { CategoryCardComponent } from './components/category-card/category-card.component';
import { SharedModule } from '../shared/shared.module';
import { ToolboxService } from './services/toolbox.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HighlightPipe } from './pipes/highlight.pipe';


@NgModule({
  declarations: [
    ToolboxComponent,
    CategoryCardComponent,
    HighlightPipe,
  ],
  imports: [
    CommonModule,
    ToolboxRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [
    ToolboxService
  ]
})
export class ToolboxModule {
}
