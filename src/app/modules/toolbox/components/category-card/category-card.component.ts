import { AfterContentInit, AfterViewInit, Component, ElementRef, Input } from '@angular/core';
import { ToolboxCategory } from '../../models/toolbox-category.model';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent {

  @Input() category: ToolboxCategory;

  @Input() set height(value: number) {
    if (!value) {
      return;
    }
    console.log(value);

    this.categoryHeightStyle = `${value}px`;
    this.heightSet = true;
  }

  @Input() searchString: string;

  heightSet = false;
  categoryHeightStyle: string;

  constructor(private elementRef: ElementRef) {
  }

  navigateToAddress(address: string) {
    window.open(address, '_blank');
  }

  get categoryHeight(): number {
    return (this.elementRef.nativeElement as HTMLElement).clientHeight;
  }

  get hasLinks(): boolean {
    return this.category.links.length > 0;
  }
}
