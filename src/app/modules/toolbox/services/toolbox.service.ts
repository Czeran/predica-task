import { Injectable } from '@angular/core';
import { ToolboxCategory } from '../models/toolbox-category.model';
import { Observable, of } from 'rxjs';

@Injectable()
export class ToolboxService {

  readonly fakeCategories: ToolboxCategory[] = [
    {
      id: '1',
      name: 'Fora dyskusyjne',
      icon: 'chat_bubble',
      links: [
        {
          id: '1',
          displayName: 'Forum Budownictwa',
          address: 'http://www.budownictwo.com'
        },
        {
          id: '2',
          displayName: 'Forum Nieruchomości',
          address: 'http://www.nieruchomosci.com'
        },
        {
          id: '3',
          displayName: 'Forum Nowości',
          address: 'http://www.nowosci.com'
        },
        {
          id: '4',
          displayName: 'Forum Instalacji',
          address: 'http://www.instalacje.com'
        }
      ]
    },
    {
      id: '2',
      name: 'Zasoby O365',
      icon: 'folder',
      links: [
        {
          id: '5',
          displayName: 'Office Apps',
          address: 'http://www.oaps.com'
        },
        {
          id: '6',
          displayName: 'OneDrive',
          address: 'http://www.onedrive.com'
        },
        {
          id: '7',
          displayName: 'Outlook',
          address: 'http://www.outlook.com'
        },
        {
          id: '8',
          displayName: 'SharePoint',
          address: 'http://www.sharepoint.com'
        },
        {
          id: '9',
          displayName: 'Yammer',
          address: 'http://www.yammer.com'
        },
      ]
    },
    {
      id: '3',
      name: 'Projekty',
      icon: 'assignment',
      links: [
        {
          id: '10',
          displayName: 'Biura',
          address: 'http://www.offices.com'
        },
        {
          id: '11',
          displayName: 'Handel',
          address: 'http://www.trade.com'
        },
        {
          id: '12',
          displayName: 'Hotele',
          address: 'http://www.hotels.com'
        },
        {
          id: '13',
          displayName: 'Kultura i Oświata',
          address: 'http://www.culture.com'
        },
        {
          id: '14',
          displayName: 'Mieszkania',
          address: 'http://www.houses.com'
        },
        {
          id: '15',
          displayName: 'Przemysł',
          address: 'http://www.industry.com'
        },
        {
          id: '16',
          displayName: 'Sport Sport',
          address: 'http://www.sport.com'
        },
      ]
    },
    {
      id: '4',
      name: 'Spółki',
      icon: 'business_center',
      links: [
        {
          id: '17',
          displayName: 'Spółka A',
          address: 'http://www.spolka.com'
        }
      ]
    },
    {
      id: '5',
      name: 'Ważne',
      icon: 'bookmark',
      links: [
        {
          id: '18',
          displayName: 'Ogłoszenia',
          address: 'http://www.announcements.com'
        }
      ]
    },
  ];

  constructor() {
  }

  getCategories$(): Observable<ToolboxCategory[]> {
    return of(this.fakeCategories);
  }
}
