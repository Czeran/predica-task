import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './core/components/header/header.component';
import { NotFoundComponent } from './core/views/not-found/not-found.component';
import { MenuComponent } from './core/components/menu/menu.component';
import { MainSearchComponent } from './core/components/main-search/main-search.component';
import { SharedModule } from './modules/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './core/components/footer/footer.component';
import { CompanyInformationComponent } from './core/components/company-information/company-information.component';
import { SidenavComponent } from './core/components/sidenav/sidenav.component';
import { SidenavService } from './core/services/sidenav.service';
import { LogoComponent } from './core/components/logo/logo.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HeaderComponent,
    MenuComponent,
    MainSearchComponent,
    FooterComponent,
    CompanyInformationComponent,
    SidenavComponent,
    LogoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SidenavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
